# PAINEL ADMINISTRATIVO EM REACT

Esse projeto contém uma aplicação *web* que está sendo desenvolvida em React 
(biblioteca JavaScript) como tecnologia base para o desenvolvimento da mesma. A 
aplicação possui telas que formam um painel administrativo que possibilita aos 
seus usuários se cadastrarem na mesma e concorrerem a uma vaga de emprego numa 
suposta empresa que faz uso dessa aplicação. Uma de suas telas possui um 
formulário de cadastro com os campos: nome, vaga, data de nascimento e e-mail. 
As principais características dessa aplicação é a de de página única, ou 
seja, Single Page Application (SPA) e a outra é a responsividade/RWD (Responsive 
Web Design — páginas da *web* sendo renderizadas bem em uma variedade de 
dispositivos e tamanhos de janelas ou telas, tais como: celulares, computadores, 
*tablets* e etc.). Este projeto (aplicação) foi desenvolvido em virtude de um 
teste (desafio) para uma vaga de emprego para uma empresa de desenvolvimento de 
soluções digitais.

## O desafio

1. Construir um painel administrativo;
2. CRUD simples;
3. Single Page Application (SPA);
4. Usar o JSON-server;
5. Pode utilizar qualquer framework/lib js ou se preferir, utilizar js puro (premissa do teste). Recomendado o Reactjs;
6. Pode utilizar qualquer biblioteca para acelerar o desenvolvimento;
7. Feedback válidos p/ serem apresentados p/ os usuários (loaders e alerts);

## Pré-requisitos

Para o desenvolvimento/implementação (novas funcionalidades) ou 
somente para rodar a aplicação localmente, é preciso apenas do [Node.js](https://nodejs.org/en/) 
instalado no seu ambiente, ou seja, no seu computador. E uma IDE (Ambiente de 
Desenvolvimento Integrado) ou um editor de código-fonte como o [VSC (Visual 
Studio Code)](https://code.visualstudio.com/) da Microsoft que roda em Windows, Linux e macOS.

### Node

O [Node](https://nodejs.org/en/) é realmente fácil de instalar e agora inclui o 
[NPM](https://www.npmjs.com/) (gerenciador de pacotes para a linguagem de 
programação JavaScript).

## Instalando o projeto (aplicação)

Após o clone do projeto ou o *download* do mesmo para a máquina, navegue através 
do terminal `cd PROJECT` e execute o comando `npm install` para configurar todos 
os passos necessários para **inicialização e visualiazação** da aplicação.

### Iniciar e visualizar

Para certificar-se que a configuração do projeto foi realizada corretamente é
preciso executar dois (2) comandos, o primeiro `json-server --watch db.json --port 3001`
que irá mostrar no terminal que o arquivo localizado na raiz do projeto 'db.json'
(arquivo usado como banco de dados para essa aplicação) foi carregado. 
O segundo comando `npm start` irá rodar a aplicação no navegador padrão escolhido
de sua preferência. **Atenção:** esses dois comandos ficam sendo executados um em 
cada janela de terminal simultaneamente!

#### Instalados

* `npm install react-loader-spinner --save`;
* `npm install --save react-router-dom`;
* `npm install axios`;
* `npm install materialize-css@next`;
* `npm install -g json-server`.

## Licença

A aplicação conta com a seguinte licença de uso:
[MIT](https://opensource.org/licenses/MIT).
